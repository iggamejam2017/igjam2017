﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningSim : MonoBehaviour {

    private SpriteRenderer sprite;
    private float targetAlpha;
    private float startAlpha;
    private float lerpTime;

    // Use this for initialization
    void Start () {
        sprite = GetComponent<SpriteRenderer>();
        StartCoroutine("LightningLoop");

	}

    IEnumerator LightningLoop()
    {
        while (true)
        {
            // Wait for time in between lightning
            yield return new WaitForSeconds(Random.Range(3f, 15f));

            // Make the sprite "flash"
            int n = Random.Range(2, 5);
            for(int i = 0; i < n; i++)
            {
                NewAlpha(Random.Range(0.5f, 1f));
                yield return new WaitForSeconds(Random.Range(0.05f, 0.15f));
                NewAlpha(0f);
                yield return new WaitForSeconds(Random.Range(0.05f, 0.15f));
            }
            
        }
        
    }

    private void NewAlpha(float a)
    {
        lerpTime = Time.time;
        targetAlpha = a;
        startAlpha = sprite.color.a;
    }

    void Update()
    {
        if(sprite.color.a != targetAlpha)
        {
            float a = Mathf.Lerp(startAlpha, targetAlpha, (Time.time - lerpTime) * 5f);
            Color c = sprite.color;
            c.a = a;
            sprite.color = c;
        }
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }


}
