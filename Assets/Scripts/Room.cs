﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Room : MonoBehaviour
{

    public List<GameObject> useableItems;
    public bool isEmpty;
    private float maximumTime; // maximum amount of time a guest will stay in the room  in milliseconds

    // Path from lobby to room (reverse for exit)
    public Transform[] waypointsToRoom;

    private Guest guest;
    private float timeLeft;
    public bool isHighlighted;
    private Glowing glowingScript;
    private Slider timeSlider;
    private GameObject timeSliderControl;

    private AudioSource TimerTickingSFX;
    private float targetSoundDistance = 4.28f;
    private float distanceFadeTime = 5f;

	private bool _openShop = false;

    void Start()
    {
        isEmpty = true;
        isHighlighted = false;
        glowingScript = GetComponentInChildren<Glowing>(true);
        glowingScript.gameObject.SetActive(false);
        timeSlider = this.GetComponentInChildren<Slider>();
        timeSliderControl =timeSlider.gameObject;
        timeSliderControl.SetActive(false);
        TimerTickingSFX = GetComponent<AudioSource>();
    }

    public void setMaximumTime(float time) {
        maximumTime = time;
    }

    public void guestEntered(GameObject go)
    {
        isEmpty = false;
        guest = go.GetComponent<Guest>();
        timeLeft = maximumTime;
        timeSliderControl.SetActive(true);
        InvokeRepeating("decreaseRemainingTime", 0.01f, 0.01f);
        TimerTickingSFX.Play();
        TimerTickingSFX.maxDistance = 25;
        StartCoroutine(FadeMaxDistance());
    }

    private IEnumerator FadeMaxDistance()
    {
        float a = 0;
        float t0 = Time.time;
        while(a < 1f)
        {
            a = (Time.time - t0) / distanceFadeTime;
            TimerTickingSFX.maxDistance = Mathf.Lerp(25, targetSoundDistance, a);
            yield return null;
        }

        TimerTickingSFX.maxDistance = targetSoundDistance;
    }

    //called by interactibles
    public void scareGuest(float fear)
    {
        if (!isEmpty && guest != null)
        {
            guest.AddFear(fear);
        }
    }


   public void guestLeft()
    {
        isEmpty = true;
        CancelInvoke("decreaseRemainingTime");
        GameObject.Find("TimeLeft").GetComponent<Slider>().gameObject.SetActive(false);
        guest.ExitRoom();
        guest = null;
        TimerTickingSFX.time = 30f;
    }

    private void decreaseRemainingTime()
    {
        timeLeft -= 1;
        timeSlider.value = timeLeft / maximumTime;
        if (timeLeft <= 0)
        {
            guestLeft();
        }
    }

    // Highlight the room when a guest needs to be Assigned if it's empty
    internal void HighlightEmpty(bool highlight)
    {
        if (isEmpty && highlight)
        {
            isHighlighted = highlight;
        }
        else
        {
            isHighlighted = false;
        }

        glowingScript.gameObject.SetActive(isHighlighted);
    }

	internal void HighlightAll(bool highlight, bool openShop)
	{
		_openShop = openShop;

		if(openShop)
			GameManager.instance.shopUI.SetActive (false);

		if (highlight)
		{
			isHighlighted = highlight;
		}
		else
		{
			isHighlighted = false;
		}

		glowingScript.gameObject.SetActive(isHighlighted);
	}

	void OnMouseDown()
    {
        
        //Debug.Log("Room " + gameObject.name + " was pressed.");
        if (isHighlighted)
        {
            isHighlighted = false;
            

			if (GameManager.instance.currentGuest) {
				isEmpty = false;
				GameManager.instance.AssignGuestToRoom (this);

				GameManager.instance.currentGuest = null;
			} else if (_openShop) {
				//Debug.Log ("Open shop for this room: " + this.gameObject.name);

				GameManager.instance.shopUI.transform.Find ("Shop").GetComponent<Shop>().room = this;
				GameManager.instance.shopUI.SetActive (true);

				foreach (GameObject go in GameManager.instance.rooms) {
					var room = go.GetComponent<Room>();
					room.HighlightEmpty(false);
				}

			}
		}

		_openShop = false;
        
    }
		
}