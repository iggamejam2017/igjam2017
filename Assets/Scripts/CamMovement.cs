﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMovement : MonoBehaviour {

    public CameraTarget[] targets;

    // Move camera to target gradually, call action after completion
    internal void MoveToTarget(string v, Action action)
    {
        foreach(var t in targets)
        {
            if(t.name.ToLower() == v.ToLower())
            {
                StopAllCoroutines();
                StartCoroutine(ZoomTo(t, action));

                return;
            }
        }

        throw new Exception("Target " + v + " not defined!");
    }

    // Move camera to target gradually
    public void MoveToTarget(string v)
    {
        MoveToTarget(v, null);
    }

    // Move camera to target instantly
    public void JumpToTarget(string v)
    {
        foreach (var t in targets)
        {
            if (t.name.ToLower() == v.ToLower())
            {
                StopAllCoroutines();

                var cam = GetComponent<Camera>();
                cam.transform.position = t.TargetPosition;
                cam.orthographicSize = t.TargetSize;

                return;
            }
        }

        throw new Exception("Target " + v + " not defined!");
    }

    IEnumerator ZoomTo(CameraTarget target, Action action)
    {
        var cam = GetComponent<Camera>();
        float start = Time.time;
        while (cam.transform.position != target.TargetPosition)
        {
            var t = (Time.time - start) * 0.5f;
            cam.transform.position = Vector3.Lerp(cam.transform.position, target.TargetPosition, t);
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, target.TargetSize, t);
            yield return null;
        }

        if(action != null)
            action();
    }

    [Serializable]
    public class CameraTarget
    {
        public string name;
        public Vector3 TargetPosition;
        public float TargetSize;
    }

}
