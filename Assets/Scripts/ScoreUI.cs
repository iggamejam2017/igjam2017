﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreUI : MonoBehaviour {

    public Text moneyText;
    public Text scoreText;
    public Text gameOverText;
    public List<Image> lives;
    public Button restart;
    bool called = false;

    public void setScore(float score) { 
        scoreText.text = score + "pts.";
    }

    public void setMoney(float money) {
        moneyText.text = "$ " + money;
    }

    private void fadeLife(Image life) {
        
    }
    IEnumerator FadeLife(Image item) {
        var tempColor = item.color;
        for (float f = 1f; f >= 0; f -= 0.1f) {
            /*
            Vector3 pos = item.GetComponent<RectTransform>().anchoredPosition;
            pos = new Vector2(pos.x, pos.y - 50);
            Debug.Log(pos);
            */

            tempColor.a = f;
            item.color = tempColor;
            yield return null;
        }
        tempColor.a = 0;
        item.color = tempColor;
    }

    public void loseLife() {
        Debug.Log("UI losing life: " + lives.Count);
        if(lives.Count >= 1) {
            Image item = lives[lives.Count - 1];
            StartCoroutine("FadeLife",item);
            lives.RemoveAt(lives.Count-1);
            if(lives.Count <= 0) {
            gameOverText.gameObject.SetActive(true);
            restart.gameObject.SetActive(true);
            }
        }
    }

    public void restartGame() {
        Debug.Log("testst");
        SceneManager.LoadScene("SceneBackup1");   
    }
}
