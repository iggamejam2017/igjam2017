﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glowing : MonoBehaviour {

    private float glowSpeed = 5f;
    private SpriteRenderer spriteRenderer;
    private bool isGlowing;

    // Use this for initialization
    void Start () {

    }

    void OnEnable()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        isGlowing = true;
        StartCoroutine(GlowingCoroutine());

    }

    void OnDisable()
    {
        isGlowing = false;
    }

    private IEnumerator GlowingCoroutine()
    {
        float c = 0;
        float t0 = Time.time;

        while (isGlowing)
        {
            c = (Time.time - t0) * glowSpeed;
            float alpha = (1 + Mathf.Sin(c)) / 2.0f;

            Color col = spriteRenderer.color;
            col.a = alpha;
            spriteRenderer.color = col;
            yield return null;
        }
    }
}
