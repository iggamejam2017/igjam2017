﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop2 : MonoBehaviour {

    public GameObject background;

	// Use this for initialization
	void Start () {
        ClearButtons();
    }



    public void ClearButtons()
    {
        for(int i = 0; i < background.transform.childCount; i++)
        {
            Destroy(background.transform.GetChild(i).gameObject);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
