﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Guest : MonoBehaviour
{

    public string guestName;

    public GuestType guestType;

    // Range size the guests fear has to be in; Used to randomize Min and Max in Start
    [Range(10, 90)]
    public float fearRange = 50;

    //[Range(0, 100)]
    private float minFearLimit = 0;
    //[Range(0, 100)]
    private float maxFearLimit = 100;

    // Speed at which Guest gets bored
    public float fearLostPerSecond = 5f;

    // Base amount of money received if everything is perfect
    public int baseReward = 50;
    public float moveSpeed = 0.5f;

    public AudioClip[] ScreamSounds;
    public RuntimeAnimatorController FearReplacementAnimator;

    private AudioSource audioSource;
    private float currentFear;
    private Animator animator;
    private bool isLoosingFear;

    private Slider fearmeter;
    private Image fearRangeImg;

    private GuestState currentState;
    private bool isMoving = false;

    private bool isFearAnimation;

    private enum GuestState
    {
        Arriving, WaitingForAssignment, MovingToRoom, ArrivedAtRoom, GettingScared, Leaving, Left
    }

    // public for testing
    public Room assignedRoom = null;

    // Use this for initialization
    void Start()
    {
        GameManager.instance.spawnBlocked = true;

        audioSource = GetComponent<AudioSource>();
        isFearAnimation = false;

        animator = GetComponent<Animator>();
        isLoosingFear = false;

        minFearLimit = UnityEngine.Random.Range(0, 100 - fearRange);
        maxFearLimit = minFearLimit + fearRange;

        fearmeter = gameObject.GetComponentInChildren<Slider>();
        fearRangeImg = transform.Find("Fearmeter").gameObject.transform.Find("Slider").transform.Find("Range").gameObject.GetComponent<Image>();

        currentFear = UnityEngine.Random.Range(0, 20);
        initFearBar();

        currentState = GuestState.Arriving;
        Transform wpReception = GameObject.Find("wpReception").transform;
        StartCoroutine(MoveThroughWaypointsCoroutine(ToVector3Array(wpReception)));
    }

    void initFearBar()
    {
        fearmeter.value = currentFear / 100;

        fearRangeImg.rectTransform.sizeDelta = new Vector2((fearRange / 100) * 70, fearRangeImg.rectTransform.sizeDelta.y);
        Vector3 pos = fearRangeImg.rectTransform.localPosition;
        fearRangeImg.rectTransform.localPosition = new Vector3(((minFearLimit / 100) * 70) - 35, pos.y, pos.z);
    }

    private Vector3[] ToVector3Array(params Transform[] t)
    {
        var result = new Vector3[t.Length];
        for (int i = 0; i < t.Length; i++)
        {
            result[i] = t[i].position;
        }

        return result;
    }

    void OnMouseDown()
    {
        //Debug.Log("Guest " + gameObject.name + " was pressed.");
        if (currentState == GuestState.WaitingForAssignment && GameManager.instance.readyForAssignment())
        {
            GameManager.instance.AwaitingAssignment(this);
        }

    }

    public void MoveToRoom(Room room)
    {
        currentState = GuestState.MovingToRoom;
        assignedRoom = room;
        StartCoroutine(MoveThroughWaypointsCoroutine(ToVector3Array(room.waypointsToRoom)));
    }

    // Moves through the waypoints; Begins at current position and moves towards first waypoint first
    private IEnumerator MoveThroughWaypointsCoroutine(params Vector3[] waypoints)
    {
        isMoving = true;
        int i = 0;
        Vector3 lastWaypoint = transform.position;
        float t0 = Time.time;
        while (i < waypoints.Length)
        {
            float distanceCovered = (Time.time - t0) * moveSpeed;
            float percentage = distanceCovered / (Vector3.Distance(lastWaypoint, waypoints[i]));
            if (float.IsNaN(percentage) || percentage >= 1.0f)
            {
                transform.position = waypoints[i];
                lastWaypoint = waypoints[i];
                t0 = Time.time;
                i++;
                continue;
            }
            //Debug.Log(percentage);

            Vector3 newPosition = Vector3.Lerp(lastWaypoint, waypoints[i], percentage);
            Vector3 delta = newPosition - transform.position;
            transform.position = newPosition;
            UpdateAnimator(lastWaypoint - waypoints[i]);

            yield return null;
        }
        isMoving = false;
        MoveComplete();
    }

    private void MoveComplete()
    {
        //Debug.Log("MoveComplete " + currentState);
        UpdateAnimator(Vector3.zero);
        switch (currentState)
        {
            case GuestState.Arriving:
                currentState = GuestState.WaitingForAssignment;

                SoundManager.instance.ReceptionBellSFX.PlaySound();
                SoundManager.instance.PlayDiscussion(guestType);
                break;
            case GuestState.MovingToRoom:
                GameManager.instance.spawnBlocked = false;
                currentState = GuestState.GettingScared;
                assignedRoom.guestEntered(gameObject);
                EnterRoom();
                RandomWalkInRoom();
                break;
            case GuestState.GettingScared:
                if (UnityEngine.Random.value > 0.5)
                    RandomWalkInRoom();
                else
                    Invoke("MoveComplete", UnityEngine.Random.Range(1f, 3f));
                break;
            case GuestState.Leaving:
                currentState = GuestState.Left;
                break;
            case GuestState.Left:
                GameManager.instance.AddMoney(calculateMoney());
                GameManager.instance.AddScore(calculateScore());
                GameManager.instance.unregisterGuest(gameObject);
                Destroy(gameObject);
                break;
        }
    }

    private float calculateScore() {
        float score = 0f;
        if(currentFear >= minFearLimit && currentFear <= maxFearLimit) {
            score = baseReward * 2;
        }else if( fearmeter.value >= 1){
            GameManager.instance.LoseLife();

        }else{
            GameManager.instance.LoseLife();

        }
        return score;
    }

    private void RandomWalkInRoom()
    {
        var poly = assignedRoom.GetComponent<PolygonCollider2D>();
        if (poly != null)
        {
            StartCoroutine(MoveThroughWaypointsCoroutine(PointInArea(poly)));
        }
    }

    public Vector3 PointInArea(PolygonCollider2D poly)
    {
        var bounds = poly.bounds;
        var center = bounds.center;

        float x = 0;
        float y = 0;
        int attempt = 0;
        do
        {
            x = UnityEngine.Random.Range(center.x - bounds.extents.x, center.x + bounds.extents.x);
            y = UnityEngine.Random.Range(center.y - bounds.extents.y, center.y + bounds.extents.y);
            attempt++;
            if (poly.OverlapPoint(new Vector2(x, y)))
                break;
        } while (attempt <= 100);
        //Debug.Log("Attemps: " + attempt);

        return new Vector3(x, y, 0);
    }

    private void UpdateAnimator(Vector3 delta)
    {

        int xDir = 0;
        int yDir = 0;

        if (delta != Vector3.zero)
        {
            // Idle Animation?
            if (Mathf.Abs(delta.x) > Mathf.Abs(delta.y))
            {
                if (delta.x > 0)
                {
                    //animator.SetTrigger("GoRight");
                    xDir = -1;
                }
                else
                {
                    //animator.SetTrigger("GoLeft");
                    xDir = 1;
                }
            }
            else
            {
                if (delta.y > 0)
                {
                    //animator.SetTrigger("GoUp");
                    yDir = -1;
                }
                else
                {
                    //animator.SetTrigger("GoDown");
                    yDir = 1;
                }
            }
        }

        animator.SetInteger("XDIR", xDir);
        animator.SetInteger("YDIR", yDir);


    }

    // Add Fear to the guest (from interactions)
    public void AddFear(float fear)
    {
        currentFear += fear;
        currentFear = Mathf.Clamp(currentFear, 0, 100);
        animator.SetTrigger("Scream");
        PlayScreamSound();
        if (!isFearAnimation)
        {
            isFearAnimation = true;
            Invoke("ReplaceAnimator", 0.5f);
        }


        fearmeter.value = currentFear / 100;
        // Some logic to determine if guest leaves
        if (fearmeter.value >= 1 ) {
            baseReward = 0;
            assignedRoom.guestLeft();
        }
    }
    private float calculateMoney(){
        if(currentFear < minFearLimit || fearmeter.value >= 1){
            baseReward = 0;
            return baseReward;
        }else{
            baseReward = 50;
            return baseReward;
        }
    }

    private void ReplaceAnimator()
    {
        animator.runtimeAnimatorController = FearReplacementAnimator;
    }

    private void PlayScreamSound()
    {
        audioSource.clip = ScreamSounds[UnityEngine.Random.Range(0, ScreamSounds.Length)];
        audioSource.Play();
    }

    private void BeginLosingFear()
    {
        isLoosingFear = true;
        //Debug.Log("begin losing fear");
        StartCoroutine(LoosingFear());
    }

    public void EnterRoom()
    {
        // Start random moving inside room
        //Debug.Log("Room entered");
        BeginLosingFear();
    }

    public void ExitRoom()
    {
        // Stop Random moving
        currentState = GuestState.Leaving;
        CancelInvoke();
        StopCoroutine(LoosingFear());
        if (isMoving)
        {
            StopCoroutine("MoveThroughWaypointsCoroutine");
        }
        else
        {
            currentState = GuestState.Left;
        }


        // Animation, sound?
        var temp = ToVector3Array(assignedRoom.waypointsToRoom);
        System.Array.Reverse(temp);
        var waypoints = new Vector3[temp.Length + 2];
        int i = 0;
        for (; i < waypoints.Length - 2; i++)
        {
            waypoints[i] = temp[i];
        }
        waypoints[i++] = GameObject.Find("wpReception").transform.position;
        waypoints[i] = GameObject.Find("SpawnPoint").transform.position;

        StartCoroutine(MoveThroughWaypointsCoroutine(waypoints));
        isLoosingFear = false; // Stops loosing fear
    }

    IEnumerator LoosingFear()
    {
        while (isLoosingFear)
        {
            if (currentFear >= 0)
            {
                currentFear -= fearLostPerSecond;
            }
            if (currentFear < 0)
            {  // fallback for case where cF = 1 and lostPerSecond > 1
                currentFear = 0;
            }
            fearmeter.value = currentFear / 100;
            // just for testing purpose
            /*
            if (fearmeter.value >= 1) {
                baseReward = 0;
                GameManager.instance.LoseLife();
                assignedRoom.guestLeft();
            }*/
            yield return new WaitForSeconds(1);
        }
    }

    //void OnValidate()
    //{
    //    minFearLimit = Mathf.Min(minFearLimit, maxFearLimit);
    //    maxFearLimit = Mathf.Max(minFearLimit, maxFearLimit);
    //}

    public enum GuestType
    {
        Man, Woman, Child
    }
}
