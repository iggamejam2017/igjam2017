﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public AudioSource musicSource;
    public AudioSource singleClipSource;

    public AudioSource ReceptionDiscussion;
    public GuestDiscussion[] guestDiscussions;

    public PlayableAudioSource ReceptionBellSFX;


    public static SoundManager instance;

    [Serializable]
    public class GuestDiscussion
    {
        public Guest.GuestType guestType;
        public AudioClip guestDiscussion;
    }

	// Use this for initialization
	void Awake () {
		if(instance == null)
        {
            instance = this;
        }else if(instance != this)
        {
            Destroy(gameObject);
        }

        //DontDestroyOnLoad(gameObject);
	}
	
    public void PlayMusic(AudioClip song)
    {
        musicSource.clip = song;
        musicSource.Play();
    }

    public void PlaySingle(AudioClip sfx)
    {
        singleClipSource.clip = sfx;
        singleClipSource.Play();
    }

    public void PlayDiscussion(Guest.GuestType type)
    {
        foreach(GuestDiscussion gd in guestDiscussions)
        {
            if(type == gd.guestType)
            {
                ReceptionDiscussion.clip = gd.guestDiscussion;
                ReceptionDiscussion.Play();
            }
        }
    }

    public void StopDiscussion()
    {
        ReceptionDiscussion.Stop();
    }

}
