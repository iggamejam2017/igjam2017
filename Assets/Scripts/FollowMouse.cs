﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouse : MonoBehaviour {

    Vector3 pos;
    public PolygonCollider2D pol;

	// Update is called once per frame
	void Update () {

        pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pos.z = 0;
        transform.position = pos;
        //Debug.Log(pol.OverlapPoint(new Vector2(pos.x, pos.y)));
	}
}
