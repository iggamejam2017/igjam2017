﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public float money = 0;
    public float baseRoomCost = 100;
    public GameObject[] guestTypes;
    public GameObject[] rooms;
    public float time = 0;
    public float highscore = 0;
    public ScoreUI scoreUI;
	public GameObject shopUI;
    private int lives = 5; 

    public AudioClip pickupGuestSFX;
    public AudioClip placeGuestSFX;
    public AudioClip getMoneySFX;

    public static GameManager instance = null;
    public Guest currentGuest;

    public List<GameObject> guests;
    public Transform spawnPoint;
    public bool spawnBlocked;
    public float maxTime;

    private CamMovement _CamMovement;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
        else if (instance != this) {
            Destroy(gameObject);
        }

        //DontDestroyOnLoad(gameObject);

        foreach (GameObject r in rooms) {
            r.GetComponent<Room>().setMaximumTime(maxTime);
        }
        _CamMovement = Camera.main.GetComponent<CamMovement>();
        _CamMovement.JumpToTarget("MainMenu");
    }

    public void StartGame()
    {
        _CamMovement.MoveToTarget("Full", StartSpawnGuests);
    }

    private void StartSpawnGuests()
    {
        StartCoroutine(SpawnGuestsCor());
    }

    IEnumerator SpawnGuestsCor() {
        while (lives > 0) { // Game loop
            if (guests.Count < rooms.Length && !spawnBlocked) {
                //Debug.Log("spawning guest");
                GameObject newGuest;
                newGuest = Instantiate(guestTypes[Random.Range(0, guestTypes.Length)], new Vector3(spawnPoint.position.x, spawnPoint.position.y, spawnPoint.position.z), Quaternion.identity);
                registerGuest(newGuest);
            }
            yield return new WaitForSeconds(0.3f);
        }
        StopCoroutine(SpawnGuestsCor());
        //TODO: GAME OVER MESSAGE
    }

    public void AddMoney(float m) {
        if(m >= 0)
            SoundManager.instance.PlaySingle(getMoneySFX);
        money += m;
        UpdateUI();
    }

    public void LoseLife() {
        lives--;
        Debug.Log("lives: " + lives);
        scoreUI.loseLife();
    }


    public void AddScore(float s) {
        highscore += s;
        UpdateUI();
    }

    public void UpdateUI() {
        scoreUI.setMoney(money);
        scoreUI.setScore(highscore);
    }

    public void AwaitingAssignment(Guest guest) {
        currentGuest = guest;
        SoundManager.instance.PlaySingle(pickupGuestSFX);

        foreach (GameObject go in rooms) {
            var room = go.GetComponent<Room>();
            room.HighlightEmpty(true);
        }
    }

    public void AssignGuestToRoom(Room assignedRoom) {
        currentGuest.GetComponent<Guest>().MoveToRoom(assignedRoom);

        SoundManager.instance.PlaySingle(placeGuestSFX);
        SoundManager.instance.StopDiscussion();

        foreach (GameObject go in rooms) {
            var room = go.GetComponent<Room>();
            room.HighlightEmpty(false);
        }
    }

	public void HighlightAllRooms(){
		foreach (GameObject go in rooms) {
			var room = go.GetComponent<Room>();
			room.HighlightAll(true, true);
		}
	}

    //returns true if any room is empty
    public bool readyForAssignment() {
        foreach (GameObject r in rooms) {
            if (r.GetComponent<Room>().isEmpty) {
                return true;
            }
        }
        return false;
    }

    void LoadHighscore() {
        highscore = PlayerPrefs.GetFloat("Highscore");
    }

    void SaveHighscore() {
        PlayerPrefs.SetFloat("Highscore", highscore);
    }

    public void registerGuest(GameObject guest) {
        guests.Add(guest);
    }
    public void unregisterGuest(GameObject guest) {
        guests.Remove(guest); //dunno if it actually removes the correct guest, but right now we're only using the list only as counter anyways
    }

    public void restart() {
        //Debug.Log("restarting...");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        /*
        highscore = 0;
        money = 0;
        lives = 5;
       */
    }


}
