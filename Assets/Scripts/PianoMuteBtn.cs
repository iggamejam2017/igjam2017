﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PianoMuteBtn : MonoBehaviour {
    
    public bool GlobalMute = false;
    public ParticleSystem particles;
    private SpriteRenderer currentSprite;

    void Awake () {
        currentSprite = gameObject.GetComponent<SpriteRenderer>();
    }
	
	void OnMouseDown()
    {
        Mute(!GlobalMute);
    }

    private void Mute(bool v)
    {
        GlobalMute = v;
        if (v)
        {
            currentSprite.color = new Color(1f, 0f, 0f, 1f);
            particles.Stop();
        }
        else
        {
            currentSprite.color = new Color(1f, 1f, 1f, 1f);
            particles.Play();
        }
    }

    void OnEnable()
    {
        Mute(GlobalMute);
    }

    void OnDisable()
    {
        if(particles != null)
            particles.Stop();
    }

}
