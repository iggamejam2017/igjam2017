﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class ItemScript : MonoBehaviour {
	public Text ItemName;
	public Text ItemCost;
	public Text FearDamage;
	public Text ItemType;
	public Text ItemDuration;

	public Text ItemUpgrade;
	public Button thisButton;
	public GameObject interactive;
	public InteractiveItem i;



	// Use this for initialization
	void Start () {

		//i = interactive.GetComponent<InteractiveItem> ();

		ItemUpgrade.enabled = false;
		Button btn = thisButton.GetComponent<Button> ();
		btn.onClick.AddListener (TaskOnClick);

		
	}

	// Update is called once per frame
	void Update () {
		Debug.Log(i);
		if (i.getCost() <= GameManager.instance.money) {
			ItemUpgrade.enabled = true;
		} else {
			ItemUpgrade.enabled = false;
		}
		
	}

	void TaskOnClick (){

		Debug.Log ("Click on item");
		if (i.getCost() <= GameManager.instance.money) {
			
			i.cost *= 2;
			i.fearDamage *= 1.5f;
            GameManager.instance.AddMoney(-i.getCost());


        }
		
	}

		


}
