﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;
using System;

public class Shop : MonoBehaviour {



	private GameObject item;
	private InteractiveItem i;


	public Room room;
	public GameObject button;

	// Use this for initialization
	void OnEnable () {

		for(int i=0; i<transform.childCount; i++){
			Destroy(transform.GetChild (i).gameObject);
		}

		foreach(GameObject usableItem in room.useableItems){
			InteractiveItem interactiveitem = usableItem.GetComponent <InteractiveItem> ();
			GameObject btn = (GameObject)Instantiate (button, transform);
			ItemScript Iscp =  btn.GetComponent<ItemScript> ();
			Iscp.interactive = interactiveitem.gameObject;
			Iscp.ItemName.text = interactiveitem.name;
			Iscp.ItemCost.text = interactiveitem.cost.ToString();
			Iscp.i = interactiveitem;

			Iscp.ItemDuration.text = interactiveitem.cooldownTimer.ToString();
			Iscp.ItemType.text = interactiveitem.getEffectTypeString();
			Iscp.FearDamage.text = interactiveitem.fearDamage.ToString();
		}
	}
	// Update is called once per frame
	void Update () {
		
	}
	public void unlockItem(){		
	}
}
