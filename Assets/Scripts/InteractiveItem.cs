﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveItem : MonoBehaviour {


	public string name ="";
	public bool isAvailable = false;
	public int cost;
	public float fearDamage = 0;
    

	private float timeUsed = 0;
	public float cooldownTimer = 0;
	private float effectDuration = 0;
	public GameObject room;
	private Room r;
	public effectType effect;
    private SpriteRenderer currentSprite;

    public Sprite OnSprite;
    public Sprite OffSprite;

    public ParticleSystem particles = null;

    private AudioSource audioSource;
    private bool firstUsed = true;

	public enum effectType {instantEffect, overTimeEffect};


    // Use this for initialization
    void Start()
    {
        r = room.GetComponent<Room>();
        audioSource = GetComponent<AudioSource>();
        currentSprite = gameObject.GetComponent<SpriteRenderer>();
        if (particles != null)
        {
            particles.Stop();
        }
    }
	public string getName(){
		return name;
	}
	public int getCost(){
		return cost;
	}

	public string getEffectTypeString(){
		switch(effect){
		case effectType.instantEffect:
			return "Instant";
		case effectType.overTimeEffect:
			return "Effect over time";
		default:
			return "Instant";
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public float getFearDamage(){
		return fearDamage;
	}

	public bool getIsAvailable(){
		return isAvailable;
	}

	float getTime(){
		return Time.time;
	}

	public bool isAvailableToUse(){
        if(currentSprite.sprite == OnSprite)
        {
            currentSprite.color = new Color(1f, 1f, 1f, 1f);
            return false;
        }

        if (firstUsed) {
            currentSprite.color = new Color(1f, 1f, 1f, 1f);
            return true;
        }
		else if (getIsAvailable() && getTime() >= timeUsed + cooldownTimer) {
            currentSprite.color = new Color(1f, 1f, 1f, 1f);
            return true;
		}
        currentSprite.color = new Color(.5f, .5f, .5f, .8f);
        return false;
	}

	public effectType getEffectType(effectType effectPara){
		if (effectPara == effectType.instantEffect) {
			return effectPara;
		} else if (effect == effectType.overTimeEffect) {
			return effectPara;
		} else {
			return effectType.instantEffect;
		}
	}

    void OnMouseDown ()
	{
		if (isAvailableToUse ()) {
            if (firstUsed) firstUsed = false;
            timeUsed = getTime ();
            if (getEffectType (effect) == effectType.instantEffect) {
				r.scareGuest (getFearDamage());
                currentSprite.sprite = OnSprite;
                audioSource.Play();
                if(particles != null)
                {
                    particles.Play();
                    Invoke("StopParticles", 5f);
                }
                Invoke("SwitchToOffSprite", 1f);
            } else if (getEffectType (effect) == effectType.overTimeEffect) {
				StartCoroutine (OvertimeEffect ());
			}
		}

	}

    public void StopParticles()
    {
        particles.Stop();
    }

    public void SwitchToOffSprite()
    {
        currentSprite.sprite = OffSprite;
    }

	IEnumerator OvertimeEffect(){

		float startEffect = getTime ();
		while (getTime() < startEffect + effectDuration) {

			r.scareGuest (getFearDamage());

			yield return new WaitForSeconds (1);
		}
	
	}

    private void LateUpdate() {
        isAvailableToUse();
    }
}
